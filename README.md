# PytermPhish v-1.0
Phishing con Asistente Voz para Termux.

⛔!!Esta herramienta fue creada con fines educativos.
Los desarrolladores no se hacen responsables por el mal uso de la misma!!⛔

Created by: Otaku-dev

Pasos de Instalación en Termux

Actualizamos Repositorios...

apt install update -y && apt upgrade -y

Instalamos Git!!...

apt install git -y

Clonamos el Repositorio...

git clone https://gitlab.com/Otaku-dev/PytermPhish

Seleccionamos el Directorio...

cd PytermPhish 

Listamos el contenido del Directorio...

ls

Damos permisos de ejecución...

chmod +x PytermPhish.py Fix-PytermPhish.py install.sh

Ejecutamos el Instalador...

bash install.sh

Ejecutamos PytermPhish...

python3 PytermPhish.py

⛔Si no funciona en tu dispositivo el modulo gTTS ejecuta el siguiente solucionador:⛔

python3 Fix-PytermPhish.py

Listo!!
